class MatchesController < ApplicationController
  include MatchesHelper
  def new
  end

  def index
    @matches = Match.all.paginate(page: params[:page], per_page: 10)
  end

  def show
    @match = Match.find(params[:id])
    @factions = %w(radiant dire)
  end

  def create
    match_id = params[:match_id]
    @api = Dota.api
    @match = @api.matches(match_id.to_i)

    add_league(@match)
    add_teams(@match)
  end

  def find_league(league_id)
    @api.leagues.each do |league|
      return league if (league.id == league_id)
    end
  end

  def add_league(match)
    league = find_league(match.league_id)
    unless new_league = League.find_by_gcleague_id(match.league_id)
      new_league =  League.create(gcleague_id: match.league_id, name: league.name, description: league.description, url: league.url)
      add_matches(match, new_league.id)
    else
      add_matches(match, new_league.id)
    end
  end

  def add_matches(match, league_id)
    unless Match.find_by_gcmatch_id(match.id)
      new_match = Match.create(gcmatch_id: match.id, league_id: league_id, type_id: match.type_id, mode_id: match.mode_id, starts_at: match.starts_at, first_blood: match.first_blood, duration: match.duration, winner: match.winner,  positive_votes: match.positive_votes, negative_votes: match.negative_votes, players_count: match.players_count)
    end
  end

  def add_teams(match)
    valid_teams = [nil, nil] # Radiant first
    valid_teams[0] = match.radiant.id unless match.radiant.id.blank?
    valid_teams[1] = match.dire.id unless match.dire.id.blank?
    valid_teams.each_with_index do |gcteam_id, index|
      if gcteam_id.nil?
        new_team = Team.create(gcteam_id: gcteam_id, name: '-')
      else
        team = @api.teams(gcteam_id)
        new_team = Team.create(gcteam_id: gcteam_id, name: team.name, tag: team.tag, country_code: team.country_code, admin_id: team.admin_id, logo_id: team.logo_id, url: team.url, team_created: team.created_at) unless new_team = Team.find_by_gcteam_id(gcteam_id)
      end

      new_match = Match.find_by_gcmatch_id(match.id)
      index == 0 ? new_match.update_attributes(radiant_team: new_team.id) : new_match.update_attributes(dire_team: new_team.id)

      add_players(match, new_team, index) # radiant is 0
    end
  end

  def add_players(match, team, faction_index)
    faction_index == 0 ? faction = :radiant : faction = :dire
    match.send(faction).players.each do |player|
      unless new_player = Player.find_by_steam_32(player.id)
        new_player = Player.create(steam_32: player.id, steam_64: convert_to_64(player.id), steam_nickname: find_nickname(player.id), team_id: team.id)
        add_details(new_player.id, player, match, faction)
      else
        add_details(new_player.id, player, match, faction)
      end
    end
  end

  def add_details(player_id, player, match, faction)
    match_id = Match.find_by_gcmatch_id(match.id)
    kda = find_kda(player.kills, player.deaths, player.assists)
    Detail.create(player_id: player_id, match_id: match_id.id, hero_id: player.hero.id, faction: faction.to_s, slot: player.slot, item_1: player.items[0].id, item_2: player.items[1].id, item_3: player.items[2].id, item_4: player.items[3].id, item_5: player.items[4].id, item_6: player.items[5].id, level: player.level, kills: player.kills, deaths: player.deaths, assists: player.assists, last_hits: player.last_hits, denies: player.denies, kda: kda, gold: player.gold, gpm: player.gpm, xpm: player.xpm, status: player.status, gold_spent: player.gold_spent, hero_damage: player.hero_damage, tower_damage: player.tower_damage, hero_healing: player.hero_healing) unless Detail.find_by(match_id: match_id, player_id: player_id)
  end
end
