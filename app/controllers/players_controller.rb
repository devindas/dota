class PlayersController < ApplicationController
  def index
    @players = Player.all.paginate(page: params[:page], per_page: 10)
  end
  def show
    @player = Player.find(params[:id])
    @player_matches = @player.matches.all.paginate(page: params[:page], per_page: 5)
  end
end
