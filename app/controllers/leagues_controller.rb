class LeaguesController < ApplicationController
  def index
    @leagues = League.all.paginate(page: params[:page], per_page: 10)
  end
  def show
    @league = League.find(params[:id])
    @league_matches = @league.matches.all.paginate(page: params[:page], per_page: 5)
    @league_teams = @league.teams.paginate(page: params[:page], per_page: 5)
  end
end
