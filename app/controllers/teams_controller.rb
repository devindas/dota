class TeamsController < ApplicationController
  def index
    @teams = Team.all.paginate(page: params[:page], per_page: 10)
  end
  def show
    @team = Team.find(params[:id])
    @team_matches = @team.matches.all.paginate(page: params[:page], per_page: 5)
  end
end
