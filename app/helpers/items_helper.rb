module ItemsHelper
  def find_item(id)
    @api = Dota.api
    detail = Detail.find(id)
    items = []
    1.upto(6) do |t|
      items.push(@api.items(detail.send("item_#{t}")).image_url) unless detail.send("item_#{t}") == 0
    end
    items
  end
end
