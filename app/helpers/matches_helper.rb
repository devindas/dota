module MatchesHelper
  def winner(name)
    if name == :dire
      @match.dire.name
    else
      @match.radiant.name
    end
  end

  def find_nickname(id)
    SteamId.new(convert_to_64(id.to_i).to_i).nickname
  rescue
    'Unknown'
  end

  def avatar(id)
    SteamId.new(convert_to_64(id.to_i).to_i).full_avatar_url
  rescue
    ''
  end

  def custom_url(id)
    'http://steamcommunity.com/profiles/' + convert_to_64(id.to_i).to_s
    end

  def convert_to_64(id)
    '765' + (id + 61_197_960_265_728).to_s
  end

  def lobby_type(id)
    case id
    when -1
      'Invalid Lobby'
    when 0
      'Public Matchmaking'
    when 1
      'Practice'
    when 2
      'Tournament'
    when 3
      'Tutorial'
    when 4
      'Co-op with Bots'
    when 5
      'Team Match'
    when 6
      'Solo Queue'
    end
  end

  def game_mode(id)
    case id
    when 0
      'None'
    when 1
      'All Pick'
    when 2
      "Captain's Mode"
    when 3
      'Random Draft'
    when 4
      'Single Draft'
    when 5
      'All Random'
    when 6
      'Intro'
    when 7
      'Diretide'
    when 8
      "Reverse Captain's Mode"
    when 9
      'The Greeviling'
    when 10
      'Tutorial'
    when 11
      'Mid Only'
    when 12
      'Least Played'
    when 13
      'New Player Pool'
    when 14
      'Compendium Matchmaking'
    when 16
      'Captains Draft'
    end
  end

  def convert_to_mins(time)
    Time.at(time).utc.strftime('%H:%M:%S')
  end

  def find_team(id)
    if team = Team.find(id)
      return team.name
    else
      return 'Invalid Team'
    end
  end

  def find_winner_team(winner, match_id)
    if winner == 'radiant'
      Team.find(Match.find(match_id).radiant_team).name
    elsif winner == 'dire'
      Team.find(Match.find(match_id).dire_team).name
    end
  end

  def find_kda(k, d, a)
    k = k.to_f
    d = d.to_f
    a = a.to_f
    if d == 0.0
      return (k + a).round(1)
    else
      return ((k + a) / d).round(1)
    end
  end

  def players(match, faction)
    Match.find(match).details.where('faction = ?', faction)
  end

  def find_faction_team(faction, match)
    if faction == 'radiant'
      return "Radiant - #{find_team(match.radiant_team)}"
    elsif faction == 'dire'
      return "Dire - #{find_team(match.dire_team)}"
    end
  end

  def faction_color(faction)
    if faction == 'radiant'
      'green-text'
    elsif faction == 'dire'
        'red-text'
    end
  end
end
