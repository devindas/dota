module HeroHelper
  def find_hero(id)
    case id
    when 1
      "Anti-Mage"
    when 2
      "Axe"
    when 3
      "Bane"
    when 4
      "Bloodseeker"
    when 5
      "Crystal Maiden"
    when 6
      "Drow Ranger"
    when 7
      "Earthshaker"
    when 8
      "Juggernaut"
    when 9
      "Mirana"
    when 10
      "Morphling"
    when 11
      "Shadow Fiend"
    when 12
      "Phantom Lancer"
    when 13
      "Puck"
    when 14
      "Pudge"
    when 15
      "Razor"
    when 16
      "Sand King"
    when 17
      "Storm Spirit"
    when 18
      "Sven"
    when 19
      "Tiny"
    when 20
      "Vengeful Spirit"
    when 21
      "Windranger"
    when 22
      "Zeus"
    when 23
      "Kunkka"
    when 25
      "Lina"
    when 26
      "Lion"
    when 27
      "Shadow Shaman"
    when 28
      "Slardar"
    when 29
      "Tidehunter"
    when 30
      "Witch Doctor"
    when 31
      "Lich"
    when 32
      "Riki"
    when 33
      "Enigma"
    when 34
      "Tinker"
    when 35
      "Sniper"
    when 36
      "Necrophos"
    when 37
      "Warlock"
    when 38
      "Beastmaster"
    when 39
      "Queen of Pain"
    when 40
      "Venomancer"
    when 41
      "Faceless Void"
    when 42
      "Wraith King"
    when 43
      "Death Prophet"
    when 44
      "Phantom Assassin"
    when 45
      "Pugna"
    when 46
      "Templar Assassin"
    when 47
      "Viper"
    when 48
      "Luna"
    when 49
      "Dragon Knight"
    when 50
      "Dazzle"
    when 51
      "Clockwerk"
    when 52
      "Leshrac"
    when 53
      "Nature's Prophet"
    when 54
      "Lifestealer"
    when 55
      "Dark Seer"
    when 56
      "Clinkz"
    when 57
      "Omniknight"
    when 58
      "Enchantress"
    when 59
      "Huskar"
    when 60
      "Night Stalker"
    when 61
      "Broodmother"
    when 62
      "Bounty Hunter"
    when 63
      "Weaver"
    when 64
      "Jakiro"
    when 65
      "Batrider"
    when 66
      "Chen"
    when 67
      "Spectre"
    when 68
      "Ancient Apparition"
    when 69
      "Doom"
    when 70
      "Ursa"
    when 71
      "Spirit Breaker"
    when 72
      "Gyrocopter"
    when 73
      "Alchemist"
    when 74
      "Invoker"
    when 75
      "Silencer"
    when 76
      "Outworld Devourer"
    when 77
      "Lycan"
    when 78
      "Brewmaster"
    when 79
      "Shadow Demon"
    when 80
      "Lone Druid"
    when 81
      "Chaos Knight"
    when 82
      "Meepo"
    when 83
      "Treant Protector"
    when 84
      "Ogre Magi"
    when 85
      "Undying"
    when 86
      "Rubick"
    when 87
      "Disruptor"
    when 88
      "Nyx Assassin"
    when 89
      "Naga Siren"
    when 90
      "Keeper of the Light"
    when 91
      "Io"
    when 92
      "Visage"
    when 93
      "Slark"
    when 94
      "Medusa"
    when 95
      "Troll Warlord"
    when 96
      "Centaur Warrunner"
    when 97
      "Magnus"
    when 98
      "Timbersaw"
    when 99
      "Bristleback"
    when 100
      "Tusk"
    when 101
      "Skywrath Mage"
    when 102
      "Abaddon"
    when 103
      "Elder Titan"
    when 104
      "Legion Commander"
    when 105
      "Techies"
    when 106
      "Ember Spirit"
    when 107
      "Earth Spirit"
    when 109
      "Terrorblade"
    when 110
      "Phoenix"
    when 111
      "Oracle"
    when 112
      "Winter Wyvern"
    end
  end
end
