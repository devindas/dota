module TeamsHelper
  def find_country(country)
    country = country.to_s
    unless country == ""
      code = IsoCountryCodes.find(country)
      code.name
    else
      "No Country Provided"
    end
  end

  def team_avatar(team)
    team_data = Steam::RemoteStorage.ugc_file(params: {ugcid: team.logo_id , appid: 570})
    team_data["data"]["url"]
  end

  def is_team_winner?(team, match)
    if team.id == match.radiant_team and match.winner == 'radiant'
      'Yes'
    elsif team.id == match.dire_team and match.winner == 'dire'
      'Yes'
    else
      'No'
    end
  end
end
