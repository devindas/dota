class Match < ActiveRecord::Base
  belongs_to :league
  has_many :details
  has_many :players, through: :details
  has_many :teams, through: :players

  def teams
    match_teams = []
    self.players.each do |player|
        match_teams.push(player.team) unless match_teams.include?(player.team)
    end
    match_teams
  end
end
