class Team < ActiveRecord::Base
  has_many :players
  has_many :details, through: :players
  has_many :matches, through: :players
  def matches
    Match.where("radiant_team = #{id}  OR dire_team = #{id}")
  end

  def longest_match
    match = {}
    match[:detail] = (self.matches.maximum('duration'))
    match[:match] = Match.where('duration = ?', match[:detail] ).first
    match
  end

  def most_kills
    match = {}
    match[:detail] = (self.details.maximum('kills'))
    match[:match] = Match.find(Detail.where('kills = ?', match[:detail]).first.match_id)
    match
  end

  def most_assists
    match = {}
    match[:detail] = (self.details.maximum('assists'))
    match[:match] = Match.find(Detail.where('assists = ?', match[:detail]).first.match_id)
    match
  end

  def most_last_hits
    match = {}
    match[:detail] = (self.details.maximum('last_hits'))
    match[:match] = Match.find(Detail.where('last_hits = ?', match[:detail]).first.match_id)
    match
  end

  def most_denies
    match = {}
    match[:detail] = (self.details.maximum('denies'))
    match[:match] = Match.find(Detail.where('denies = ?', match[:detail]).first.match_id)
    match
  end

  def most_gold
    match = {}
    match[:detail] = (self.details.maximum('gold'))
    match[:match] = Match.find(Detail.where('gold = ?', match[:detail]).first.match_id)
    match
  end

  def most_hero_damage
    match = {}
    match[:detail] = (self.details.maximum('hero_damage'))
    match[:match] = Match.find(Detail.where(' hero_damage = ?', match[:detail]).first.match_id)
    match
  end

  def most_hero_healing
    match = {}
    match[:detail] = (self.details.maximum('hero_healing'))
    match[:match] = Match.find(Detail.where('hero_healing = ?', match[:detail]).first.match_id)
    match
  end

  def most_tower_damage
    match = {}
    match[:detail] = (self.details.maximum('tower_damage'))
    match[:match] = Match.find(Detail.where('tower_damage = ?', match[:detail]).first.match_id)
    match
  end

  def best_kda
    match = {}
    match[:detail] = (self.details.maximum('kda'))
    match[:match] = Match.find(Detail.where('kda = ?', match[:detail]).first.match_id)
    match
  end

  def highest_gpm
    match = {}
    match[:detail] = (self.details.maximum('gpm'))
    match[:match] = Match.find(Detail.where('gpm = ?', match[:detail]).first.match_id)
    match
  end

  def highest_xpm
    match = {}
    match[:detail] = (self.details.maximum('xpm'))
    match[:match] = Match.find(Detail.where('xpm = ?', match[:detail]).first.match_id)
    match
  end
end
