class League < ActiveRecord::Base
  has_many :matches
  has_many :details, through: :matches
  has_many :players, through: :matches

  def teams
    league_teams = []
    Match.where("league_id = ?", id).each do |match|
      match.teams.each do |team|
        league_teams.push(team) unless league_teams.include?(team)
      end
    end
    league_teams
  end

  def longest_match
    match = {}
    match[:detail] = (self.matches.maximum('duration'))
    match[:match] = Match.where("duration = ? AND league_id = #{id}", match[:detail] ).first
    match
  end

  def most_kills
    match = {}
    match[:detail] = (self.details.maximum('kills'))
    match[:match] = Match.find(self.details.where('kills = ?', match[:detail]).first.match_id)
    match
  end

  def most_assists
    match = {}
    match[:detail] = (self.details.maximum('assists'))
    match[:match] = Match.find(self.details.where('assists = ?', match[:detail]).first.match_id)
    match
  end

  def most_last_hits
    match = {}
    match[:detail] = (self.details.maximum('last_hits'))
    match[:match] = Match.find(self.details.where('last_hits = ?', match[:detail]).first.match_id)
    match
  end

  def most_denies
    match = {}
    match[:detail] = (self.details.maximum('denies'))
    match[:match] = Match.find(self.details.where('denies = ?', match[:detail]).first.match_id)
    match
  end

  def most_gold
    match = {}
    match[:detail] = (self.details.maximum('gold'))
    match[:match] = Match.find(self.details.where('gold = ?', match[:detail]).first.match_id)
    match
  end

  def most_hero_damage
    match = {}
    match[:detail] = (self.details.maximum('hero_damage'))
    match[:match] = Match.find(self.details.where(' hero_damage = ?', match[:detail]).first.match_id)
    match
  end

  def most_hero_healing
    match = {}
    match[:detail] = (self.details.maximum('hero_healing'))
    match[:match] = Match.find(self.details.where('hero_healing = ?', match[:detail]).first.match_id)
    match
  end

  def most_tower_damage
    match = {}
    match[:detail] = (self.details.maximum('tower_damage'))
    match[:match] = Match.find(self.details.where('tower_damage = ?', match[:detail]).first.match_id)
    match
  end

  def best_kda
    match = {}
    match[:detail] = (self.details.maximum('kda'))
    match[:match] = Match.find(self.details.where('kda = ?', match[:detail]).first.match_id)
    match
  end

  def highest_gpm
    match = {}
    match[:detail] = (self.details.maximum('gpm'))
    match[:match] = Match.find(self.details.where('gpm = ?', match[:detail]).first.match_id)
    match
  end

  def highest_xpm
    match = {}
    match[:detail] = (self.details.maximum('xpm'))
    match[:match] = Match.find(self.details.where('xpm = ?', match[:detail]).first.match_id)
    match
  end

end
