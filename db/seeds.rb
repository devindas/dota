# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
require 'matches_controller'
include MatchesHelper

c = MatchesController.new
api = Dota.api
matches = api.matches(league_id: 3750)
matches.each_with_index do |match, index|
  puts "#{match.id} -- #{index}"
  c.create(match.id)
end
