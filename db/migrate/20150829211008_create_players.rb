class CreatePlayers < ActiveRecord::Migration
  def change
    create_table :players do |t|
      t.integer :steam_32
      t.float :steam_64
      t.string :steam_nickname
      t.string :name
      t.belongs_to :team, index: true
      t.timestamps null: false
    end
  end
end
