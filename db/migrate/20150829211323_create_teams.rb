class CreateTeams < ActiveRecord::Migration
  def change
    create_table :teams do |t|
      t.string :name
      t.integer :gcteam_id
      t.string :tag
      t.string :country_code
      t.integer :admin_id
      t.bigint :logo_id
      t.string :url
      t.datetime :team_created
      t.timestamps null: false
    end
  end
end
