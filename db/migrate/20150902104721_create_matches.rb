class CreateMatches < ActiveRecord::Migration
  def change
    create_table :matches do |t|
      t.belongs_to :league
      t.integer :gcmatch_id
      t.integer :league_id
      t.integer :type_id
      t.integer :mode_id
      t.timestamp :starts_at
      t.integer :first_blood
      t.integer :duration
      t.integer :radiant_team
      t.integer :dire_team
      t.string :winner
      t.integer :positive_votes
      t.integer :negative_votes
      t.integer :players_count
      t.integer :ban_1
      t.integer :ban_2
      t.integer :ban_3
      t.integer :ban_4
      t.integer :ban_5
      t.integer :ban_6
      t.integer :ban_7
      t.integer :ban_8
      t.integer :ban_9
      t.integer :ban_10
      t.integer :pick_1
      t.integer :pick_2
      t.integer :pick_3
      t.integer :pick_4
      t.integer :pick_5
      t.integer :pick_6
      t.integer :pick_7
      t.integer :pick_8
      t.integer :pick_9
      t.integer :pick_10
      t.string :first_pick
      t.timestamps null: false
    end
  end
end
