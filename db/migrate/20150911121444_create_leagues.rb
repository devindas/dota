class CreateLeagues < ActiveRecord::Migration
  def change
    create_table :leagues do |t|
      t.integer :gcleague_id, index: true
      t.string :name
      t.string :description
      t.string :url
      t.timestamps null: false
    end
  end
end
