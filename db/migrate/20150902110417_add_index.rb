class AddIndex < ActiveRecord::Migration
  def change
    add_index :matches, :gcmatch_id, unique: true
    add_index :teams, :gcteam_id, unique: true
    add_index :players, :steam_32, unique: true
  end
end
