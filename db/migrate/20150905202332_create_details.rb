class CreateDetails < ActiveRecord::Migration
  def change
    create_table :details do |t|
      t.belongs_to :player, index: true
      t.belongs_to :match, index: true
      t.integer :hero_id
      t.string :faction
      t.integer :slot
      t.integer :item_1
      t.integer :item_2
      t.integer :item_3
      t.integer :item_4
      t.integer :item_5
      t.integer :item_6
      t.integer :level
      t.integer :kills
      t.integer :deaths
      t.integer :assists
      t.float :kda
      t.integer :last_hits
      t.integer :denies
      t.integer :gold
      t.integer :gpm
      t.integer :xpm
      t.string :status
      t.integer :gold_spent
      t.integer :hero_damage
      t.integer :tower_damage
      t.integer :hero_healing
      t.timestamps null: false
    end
    add_index :details, [:player_id, :match_id], unique: true
  end
end
