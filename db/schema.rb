# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150911121444) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "details", force: :cascade do |t|
    t.integer  "player_id"
    t.integer  "match_id"
    t.integer  "hero_id"
    t.string   "faction"
    t.integer  "slot"
    t.integer  "item_1"
    t.integer  "item_2"
    t.integer  "item_3"
    t.integer  "item_4"
    t.integer  "item_5"
    t.integer  "item_6"
    t.integer  "level"
    t.integer  "kills"
    t.integer  "deaths"
    t.integer  "assists"
    t.float    "kda"
    t.integer  "last_hits"
    t.integer  "denies"
    t.integer  "gold"
    t.integer  "gpm"
    t.integer  "xpm"
    t.string   "status"
    t.integer  "gold_spent"
    t.integer  "hero_damage"
    t.integer  "tower_damage"
    t.integer  "hero_healing"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  add_index "details", ["match_id"], name: "index_details_on_match_id", using: :btree
  add_index "details", ["player_id", "match_id"], name: "index_details_on_player_id_and_match_id", unique: true, using: :btree
  add_index "details", ["player_id"], name: "index_details_on_player_id", using: :btree

  create_table "leagues", force: :cascade do |t|
    t.integer  "gcleague_id"
    t.string   "name"
    t.string   "description"
    t.string   "url"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  add_index "leagues", ["gcleague_id"], name: "index_leagues_on_gcleague_id", using: :btree

  create_table "matches", force: :cascade do |t|
    t.integer  "league_id"
    t.integer  "gcmatch_id"
    t.integer  "type_id"
    t.integer  "mode_id"
    t.datetime "starts_at"
    t.integer  "first_blood"
    t.integer  "duration"
    t.integer  "radiant_team"
    t.integer  "dire_team"
    t.string   "winner"
    t.integer  "positive_votes"
    t.integer  "negative_votes"
    t.integer  "players_count"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
  end

  add_index "matches", ["gcmatch_id"], name: "index_matches_on_gcmatch_id", unique: true, using: :btree

  create_table "players", force: :cascade do |t|
    t.integer  "steam_32"
    t.float    "steam_64"
    t.string   "steam_nickname"
    t.string   "name"
    t.integer  "team_id"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
  end

  add_index "players", ["steam_32"], name: "index_players_on_steam_32", unique: true, using: :btree
  add_index "players", ["team_id"], name: "index_players_on_team_id", using: :btree

  create_table "teams", force: :cascade do |t|
    t.string   "name"
    t.integer  "gcteam_id"
    t.string   "tag"
    t.string   "country_code"
    t.integer  "admin_id"
    t.integer  "logo_id",      limit: 8
    t.string   "url"
    t.datetime "team_created"
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  add_index "teams", ["gcteam_id"], name: "index_teams_on_gcteam_id", unique: true, using: :btree

end
