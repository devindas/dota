Rails.application.routes.draw do
  root to: 'matches#new'
  resources :matches, only: [:create, :new, :index, :show]
  resources :players, only: [:show, :index]
  resources :teams, only: [:show, :index]
  resources :leagues, only: [:show, :index]
end
